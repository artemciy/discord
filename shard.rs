#![feature (non_ascii_idents)]
#![allow (unknown_lints, uncommon_codepoints)]

#[macro_use] extern crate alloc;
#[macro_use] extern crate gstuff;

use gstuff::filename;
use inlinable_string::{InlinableString, StringExt};
use std::fmt::{self, Write as FmtWrite};

// TBD: A library/command that would connect to a shard and stream/print the events
// Use the command to troubleshoot the shards,
// use the library in place of https://github.com/SpaceManiac/discord-rs in Sidekick

// TBD: Log the “session_start_limit”

// TBD: Use the “session_start_limit” in Sidekick to keep us from being banned

// TBD: Implement *mirroring*: running a shard under a different bot ID,
// but using a distributed discovery to find and use the shared Sidekick engine
// Concerns: automatically updating the driver
// Consider implementing a service for automatically renting a mirror VPS

pub fn _log (file: &'static str, line: u32, is: InlinableString, rc: Result<(), fmt::Error>) {
  if let Err (err) = rc {eprint! ("!write!: {}", err); return}
  println! ("{}:{}] {}", filename (file), line, is)}

/// Only using the `InlinableString` small string optimization here  
/// A larger buffer [might take a performance hit on a VPS](https://gitlab.com/artemciy/lin-socks/-/blob/95d2bb96/bench/stack-string.rs#L19)
/// and might be less than helpful with [small stacks](https://gitlab.com/artemciy/lin-socks/-/blob/aaa2d58e/src/lin-socks.rs#L88)  
/// Also using `write!` rather than `fomat-macros` in order to lean towards better `no_std` compatibility in the codebase
#[macro_export]
macro_rules! log {
  ($($args: tt)+) => {{
    let mut is = InlinableString::new();
    let rc = write! (&mut is, $($args)+);
    $crate::_log (file!(), line!(), is, rc)}}}

fn main() -> Result<(), String> {
  let (gateway, limit) = try_s! (discord::gateway());
  log! ("{:?}", gateway);
  log! ("limit: {:?}", limit);
  Ok(())}
