#![feature (non_ascii_idents)]
#![allow (unknown_lints, uncommon_codepoints)]

#[macro_use] extern crate alloc;
#[macro_use] extern crate gstuff;
#[macro_use] extern crate serde_derive;

use gstuff::{binprint, slurp};
use inlinable_string::{InlinableString/*, StringExt*/};
use serde_json::{self as json};

/// https://github.com/discord/discord-api-docs/blob/master/docs/topics/Rate_Limits.md
#[derive(Debug)]
pub struct RateLimit {
  /// X-RateLimit-Limit
  limit: u32,
  /// X-RateLimit-Remaining
  remaining: u32,
  /// X-RateLimit-Reset
  reset: f64,
  /// X-RateLimit-Reset-After
  reset_after: f64,
  /// X-RateLimit-Bucket
  bucket: InlinableString}

#[derive(Debug, Deserialize)]
pub struct SessionStartLimit {
  pub total: u32,
  pub remaining: u32,
  pub reset_after: u32,
  pub max_concurrency: u32}

#[derive(Debug, Deserialize)]
pub struct Gateway {
  pub url: InlinableString,
  pub shards: u32,
  pub session_start_limit: SessionStartLimit}

pub fn gateway() -> Result<(Gateway, Option<RateLimit>), String> {
  // TODO: Configurable auth provider
  let home = try_s! (dirs::home_dir().ok_or ("!home_dir"));
  let authᵖ = home.join (".discord") .join ("Authorization");
  let auth = binprint (&slurp (&authᵖ), b'?');
  let auth = auth.trim();
  if auth.is_empty() || !auth.starts_with ("Bot ") {return ERR! ("! {}", authᵖ.display())}

  // https://discord.com/developers/docs/topics/gateway#connecting
  // “The first step in establishing connectivity to the gateway is requesting a valid websocket endpoint from the API”
  // https://discord.com/developers/docs/topics/gateway#get-gateway-bot
  let url = "https://discord.com/api/v8/gateway/bot";
  let mut get = attohttpc::get (url);
  get = get.header ("Authorization", auth);
  let resp = try_s! (get.send());
  let succ = resp.is_success();

  // https://github.com/discord/discord-api-docs/blob/master/docs/topics/Rate_Limits.md
  let hdr = resp.headers();
  let limit = hdr.get ("X-RateLimit-Limit");
  let remaining = hdr.get ("X-RateLimit-Remaining");
  let reset = hdr.get ("X-RateLimit-Reset");
  let reset_after = hdr.get ("X-RateLimit-Reset-After");
  let bucket = hdr.get ("X-RateLimit-Bucket");
  let limit = if let (Some (limit), Some (remaining), Some (reset), Some (reset_after), Some (bucket)) = (limit, remaining, reset, reset_after, bucket) {
    Some (RateLimit {
      limit: try_s! (try_s! (limit.to_str()) .parse()),
      remaining: try_s! (try_s! (remaining.to_str()) .parse()),
      reset: try_s! (try_s! (reset.to_str()) .parse()),
      reset_after: try_s! (try_s! (reset_after.to_str()) .parse()),
      bucket: try_s! (bucket.to_str()) .into()})
  } else {None};

  let bytes = try_s! (resp.bytes());
  if !succ {return ERR! ("!gateway: {}", binprint (&bytes, b'.'))}

  let gateway: Gateway = try_s! (json::from_slice (&bytes));
  Ok ((gateway, limit))}

// TODO: use discord::model::{ChannelId, ChannelType, MessageId, Message, OnlineStatus,
//  Presence, Reaction, ReactionEmoji, ServerId, UserId};
// use discord::builders::{EmbedBuilder, EmbedFieldsBuilder};
// use discord::model::{Channel, ChannelId, ChannelType, Event, LiveServer, Message, MessageId, MessageType,
//  PossibleServer, Reaction, ReactionEmoji, ServerId, UserId, User};

// https://discord.com/developers/docs/reference

//   pub static ref BOT: Arc<Discord> = Arc::new (unwrap! (Discord::from_bot_token (TOKEN)));
//     let bot = try_s! (Discord::from_bot_token (TOKEN));
// Ok(Discord::from_token_raw(format!("Bot {}", token.trim())))

//     let (mut dc, _ready_ev) = match bot.connect_sharded (shard_id, total_shards) {
  // https://discord.com/developers/docs/topics/gateway#connecting
  // The first step in establishing connectivity to the gateway is requesting a valid websocket endpoint from the API
  // https://discord.com/developers/docs/topics/gateway#get-gateway-bot
// Note the session_start_limit
//  "url": "wss://gateway.discord.gg/",

//       let event = dc.recv_event();
